$(function() {

  var ball = ('#ball');
  var paddle = ('.paddle');
  var player_paddle = ('#player_paddle');
  var player2_paddle = ('#player2_paddle');
  var pong = ('#pong');

  var paddle_width = parseInt(paddle.width());
  var paddle_initial_position = parseInt(paddle.css('left'));
  var ball_left = parseInt(ball.css('left'));
  var ball_height = parseInt(ball.height());
  var ball_width = parseInt(ball.width());
  var pong_height = parseInt(pong.height());
  var pong_width = parseInt(pong.width());

  var go_up = false;
  var go_down = true;
  var bleft;
  var pleft;
  var ball_go = 'down';
  var ball_right_left = 'right';
  var top_angle = 6;
  var right_left_angle = 0;
  
  var move_right = false;
  var move_left = false;
  
  var move_rightp1 = false;
  var move_leftp1 = false;

  var game_over = false;
  var won;
  var anime_id;
  var restart_btn = $('#restart_btn');

  function repeat(){

    if(game_over === false){
    
      if (collision(ball, player_paddle)) {

        bleft = parseInt(ball.css('left')) + ball_width / 2;
        pleft = parseInt(player_paddle.css('left')) + paddle_width / 2;
        ball_right_left = (bleft > pleft ? 'right' : 'left');
        right_left_angle = Math.abs((pleft - bleft)) / 7; //console.log(right_left_angle);
        ball_go = 'down';

      }else if (collision(ball, player2_paddle)) {

        bleft = parseInt(ball.css('left')) + ball_width / 2;
        pleft = parseInt(player2_paddle.css('left')) + paddle_width / 2;
        ball_right_left = (bleft > pleft ? 'right' : 'left');
        right_left_angle = Math.abs((pleft - bleft)) / 7; //console.log(right_left_angle);
        ball_go = 'up';

      }else if (parseInt(ball.css('left')) <=0){

        ball_right_left = 'right';

      }else if (parseInt(ball.css('left')) >= pong_width - ball_width){

        ball_right_left = 'left';
    
      }else if (parseInt(ball.css('top')) <= 0) {

        won = 1;
        stop_the_game();

      }else if (parseInt(ball.css('top')) >= (pong_height - ball_height)){

        won = 2;
        stop_the_game();

      }

      if (ball_go === 'down'){
      ball_down();
      }else if (ball_go === 'up'){
      ball_up();
      }

      anime_id = requestAnimationFrame(repeat);
  
    }
  }


  anime_id = requestAnimationFrame(repeat);

  function left() {
    if (parseInt(player2_paddle.css('left')) > 0) {
        player2_paddle.css('left', parseInt(player2_paddle.css('left')) - 15);
        move_left = requestAnimationFrame(left);
    }
  }
    
  function right() {
      if (parseInt(player2_paddle.css('left')) < (pong_width - paddle_width)) {
          player2_paddle.css('left', parseInt(player2_paddle.css('left')) + 15);
          move_right = requestAnimationFrame(right);
      }
  }

  function leftp1() {
    if (parseInt(player_paddle.css('left')) > 0) {
        player_paddle.css('left', parseInt(player_paddle.css('left')) - 15);
        move_leftp1 = requestAnimationFrame(leftp1);
    }
  }
  
  function rightp1() {
    if (parseInt(player_paddle.css('left')) < (pong_width - paddle_width)) {
        player_paddle.css('left', parseInt(player_paddle.css('left')) + 15);
        move_rightp1 = requestAnimationFrame(rightp1);
    }
  }


  $(document).on('keydown', function (e) {
      var key = e.keyCode;
      if (key === 37 && move_left === false && game_over === false) {
       
          move_left = requestAnimationFrame(left);
    
      } else if (key === 39 && move_right === false && game_over === false) {
          
          move_right = requestAnimationFrame(right);

      } else if (key === 65 && move_leftp1 === false && game_over === false) {
         
          move_leftp1 = requestAnimationFrame(leftp1);

      } else if (key === 83 && move_rightp1 === false && game_over === false) {
          
          move_rightp1 = requestAnimationFrame(rightp1);

      }
  });

  $(document).on('keyup', function (e) {
    var key = e.keyCode;
    if (key === 37 && game_over === false) {
        
        cancelAnimationFrame(move_left);
        move_left = false;

    } else if (key === 39 && game_over === false) {
        
        cancelAnimationFrame(move_right);
        move_right = false;

    } else if (key === 65 && game_over === false) {
        
        cancelAnimationFrame(move_leftp1);
        move_leftp1 = false;

    } else if (key === 83 && game_over === false) {
        
        cancelAnimationFrame(move_rightp1);
        move_rightp1 = false;
        
    }
  });

  function ball_up() {
    ball.css('top', parseInt(ball.css('top')) - (top_angle));
    if (ball_right_left === 'left') {
        
        ball.css('left', parseInt(ball.css('left')) - (right_left_angle));
        
    } else {
        
        ball.css('left', parseInt(ball.css('left')) + (right_left_angle));

    }
  }

  function ball_down() {
    ball.css('top', parseInt(ball.css('top')) + (top_angle));
    if (ball_right_left === 'left') {
        
        ball.css('left', parseInt(ball.css('left')) - (right_left_angle));

    } else {
        
      ball.css('left', parseInt(ball.css('left')) + (right_left_angle));

    }
  } 

  function stop_the_game() {

    cancelAnimationFrame(anime_id);
    cancelAnimationFrame(move_right);
    cancelAnimationFrame(move_left);
    cancelAnimationFrame(move_rightp1);
    cancelAnimationFrame(move_leftp1);
    game_over = true;
    restart_btn.html('<span>Player ' + won + ' won</span><br><br><span id="winner">Restart</span>').show();

  }

  $(document).on('click', '#restart_btn', function () {
    location.reload();
  });

  function collision($div1, $div2) {
    var x1 = $div1.offset().left;
    var y1 = $div1.offset().top;
    var h1 = $div1.outerHeight(true);
    var w1 = $div1.outerWidth(true);
    var b1 = y1 + h1;
    var r1 = x1 + w1;
    var x2 = $div2.offset().left;
    var y2 = $div2.offset().top;
    var h2 = $div2.outerHeight(true);
    var w2 = $div2.outerWidth(true);
    var b2 = y2 + h2;
    var r2 = x2 + w2;

    if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
    return true;
  }

});